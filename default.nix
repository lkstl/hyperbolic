{ pkgs ? import <nixpkgs> {}, ref ? "main", rev ? null, useCwd ? false }:
let
  src = if useCwd then ./. else (
    fetchGit ( {
        url = ./.;
        inherit ref;
      } // (
        if rev != null then { inherit rev; } else {}
        )
      )
    );
  tex = pkgs.texlive.combine ( {
      inherit (pkgs.texlive) scheme-basic collection-luatex fontspec;
    } // texPackages );
  texPackages = { inherit (pkgs.texlive)
      latexmk
      xcolor
      scalerel
      mathtools etoolbox
      faktor
      pgf tikz-cd
      cleveref
      ; };
in pkgs.runCommand "xaverdh-hyperbolic" {
     buildInputs = [ tex ];
   } ''
    mkdir $out
    export TEXMFVAR="$NIX_BUILD_TOP"
    cd ${src}
    latexmk -lualatex -output-directory="$NIX_BUILD_TOP" -aux-directory="$NIX_BUILD_TOP" -jobname=result main.tex
    cp "$NIX_BUILD_TOP"/result.pdf $out/hyperbolic.pdf
  ''
